from fastapi import APIRouter, Header, HTTPException
from app.model.user import User
from app.service.user_service import UserService
from typing import Optional
from app.exception.user_not_authenticated_exception import UserNotAuthenticated

router = APIRouter()


@router.post("/restaurants/", tags=["restaurants"])
def get_restaurants(username: Optional[str] = Header(None), password: Optional[str] = Header(None)):
    try:
        user = UserService.authenticate_and_get_user(
            username=username, password=password)
        print(user)
        # call your service here
    except UserNotAuthenticated:
        raise HTTPException(status_code=401, detail="User must be logged")
