DROP TABLE IF EXISTS users CASCADE;

CREATE TABLE users (
    id_users serial PRIMARY KEY,
    username text UNIQUE NOT NULL,
    password text NOT NULL
);